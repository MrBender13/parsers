"""
Parser apartments from the site "Мой-Адрес.рф".

To start the run:

    scrapy runspider scraper/spiders/spider.py

See more details https://github.com/f213/rumetr-scraping-tutorial.
"""

import hashlib
import json
import re
from urllib.parse import urlencode
import scrapy
from rumetr.scrapy import ApptItem as Item


class Spider(scrapy.Spider):
    """Spider class"""
    name = 'spider'
    start_urls = ['http://corp.samoletgroup.ru/']

    def parse(self, response):
        """Parse main site page, extract urls to sub sites"""
        hrefs = response.xpath('//div[@class="column_attr align_center"]/div/a/@href').extract()[1:]
        hrefs = set(hrefs)
        hrefs ^= set(['http://samoletgroup.ru/build/'])

        for href in hrefs:
            url = href.replace('www.', '').strip('/')
            name = self.get_complex_name(url)

            meta = {
                'complex_id': self.get_hash(name),
                'complex_name': name,
                'complex_url': url,
                'addr': '',
                'is_v2_search': False,
                'body': {
                    'sort': 'price',
                    'order': 'asc',
                    'page': 1,
                }
            }

            v2_search_urls = ['http://tomilinosamolet.ru', 'http://prigorod77.ru']
            if url in v2_search_urls:
                ajax_url = url + '/ajax/search_v2.php'
                meta['is_v2_search'] = True
                meta['body']['object'] = 1
            else:
                ajax_url = url + '/ajax/search.php'
                meta['body']['object'] = 11

                if url not in ['http://vnukovosamolet.ru', 'http://sputnikcity.ru']:
                    meta['body']['apart[all]'] = 'on'

                if url in ['http://sputnikcity.ru']:
                    meta['body']['type'] = 1

            yield scrapy.Request(
                ajax_url,
                method='POST',
                callback=self.parse_sub_site,
                headers={
                    'Accept': 'application/json, text/javascript',
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                body=urlencode(meta['body']),
                meta=meta
            )

    def parse_sub_site(self, response):
        """Parse sub site page"""
        data = json.loads(response.text)
        data = next(iter(data['building'].values()))
        flats = data['flats']
        meta = response.meta
        v2_search = meta['is_v2_search']
        next_page = int(data['next_page'])

        for flat in flats:
            if v2_search:
                item = self.build_item_for_v2_search(flat, meta)
            else:
                item = self.build_item_for_v1_search(flat, meta)

            if item is not None:
                yield item

        if next_page != 0:
            meta['body']['page'] = next_page

            yield scrapy.Request(
                response.url,
                method='POST',
                callback=self.parse_sub_site,
                headers={
                    'Accept': 'application/json, text/javascript',
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                body=urlencode(meta['body']),
                meta=meta
            )

    def build_item_for_v1_search(self, flat, meta):
        """Build item for v1"""
        if 'reserved' not in flat:
            return None

        if flat['reserved']:
            return None

        item = Item()
        building = flat['building']
        section = flat['section']
        house_name = 'Корпус {}, секция {}'.format(building, section)
        room_count = flat['roomsNum'].replace('к', '')
        try:
            room_count = int(room_count)
        except ValueError:
            room_count = 1

        item['complex_id'] = meta['complex_id']
        item['complex_name'] = meta['complex_name']
        item['complex_url'] = meta['complex_url']
        item['addr'] = meta['addr']
        item['house_id'] = self.get_hash(house_name)
        item['house_name'] = house_name
        item['house_url'] = item['complex_url']
        item['id'] = flat['id']
        item['floor'] = flat['floor']
        item['room_count'] = room_count
        item['is_studio'] = not flat['notStudio']
        item['square'] = flat['totalArea']
        try:
            item['price'] = flat['price'].replace(' ', '')
        except KeyError:
            return None

        return item

    def build_item_for_v2_search(self, flat, meta):
        """Build item for v2"""
        if 'STATUS' not in flat:
            return None

        if int(flat['STATUS']) != 1:
            return None

        item = Item()
        building_id = flat['BUILDING']['ID']
        building_name = flat['BUILDING']['ALIAS']
        section_id = flat['SECTION']['ID']
        section_name = flat['SECTION']['NAME']
        house_id = '{}-{}'.format(building_id, section_id)
        house_name = 'Корпус {}, секция {}'.format(building_name, section_name)

        item['complex_id'] = meta['complex_id']
        item['complex_name'] = meta['complex_name']
        item['complex_url'] = meta['complex_url']
        item['addr'] = meta['addr']
        item['house_id'] = house_id
        item['house_name'] = house_name
        item['house_url'] = item['complex_url']
        item['id'] = flat['ID']
        item['floor'] = flat['FLOOR']['NAME']
        item['room_count'] = flat['ROOMS']
        item['is_studio'] = int(flat['TYPE']) == 1
        item['square'] = flat['AREA_TOTAL']
        item['price'] = flat['price'].replace(' ', '')

        return item

    @staticmethod
    def get_hash(value):
        """Return md5 hash"""
        return hashlib.md5(value.encode('utf-8')).hexdigest()

    @staticmethod
    def strip_tags(html):
        """Strip html tags"""
        return re.sub(r'<.*?>', '', html)

    @staticmethod
    def get_complex_name(url):
        # FIXME: I need an idea how to pull out the name of a complex from svg.
        db = {
            'https://himkisamolet.ru': 'Химки 2019',
            'http://tomilinosamolet.ru': 'Томилино',
            'http://lubercysamolet.ru': 'Люберцы 2017',
            'http://sputnikcity.ru': 'Спутник',
            'http://vnukovosamolet.ru': 'Внуково 2017',
            'http://prigorod77.ru': 'Пригород Лесное',
        }

        return db.get(url, None)
