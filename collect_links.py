import requests
import random
from bs4 import BeautifulSoup
import lxml.html
import os
import time
from multiprocessing import Process, Lock
from multiprocessing import Array, Value
import logging


# setting up logging
logging.basicConfig(format=u'%(levelname)-8s [%(asctime)s] %(message)s',
                    level=logging.WARNING)

PARTS_TO_SPLIT = 5
# setting constants
HOME_URL = 'https://www.linkedin.com/directory/people-{symbol}/'
BASIC_URL = 'https://www.linkedin.com'
LETTER = 'a'
ROOT_DIR = os.getcwd()
lock = Lock()
ALIVE_PROCESSES = 0
CLOSE_PROCESS = -1
OK = 0

# at list 10 accounts, because script will split data on 10 parts
# or it will split data on parts by length of ACCOUNTS
ACCOUNTS = []


# class to manage sessions
# todo check failed login
# todo add messages
# todo think on variables
class SessionsPool:
    def __init__(self, accounts):
        self._pool_size = Value('i', len(accounts))
        self._accounts = accounts
        random.shuffle(self._accounts)
        self._sessions = list()
        self._busy = Value('i', 0)
        self._lock = Lock()

        bitmap = []
        for i in range(self._pool_size.value):
            time.sleep(5)
            try:
                # start session
                new_session = requests.session()
                # loading login page
                page = lxml.html.fromstring(new_session.get('https://www.linkedin.com/').content)
                # searching login form
                form = page.forms[0]
                # loading login and password
                form.fields['session_key'] = self._accounts[i]['log']
                form.fields['session_password'] = self._accounts[i]['pass']

                new_session.post(form.action, data=form.form_values(), timeout=40)
                self._sessions.append({'log': self._accounts[i]['log'], 'session': new_session})
                bitmap.append(0)
                print('Successfully logged in {}'.format(self._accounts[i]['log']))

            except Exception:
                logging.error('Failed to login {}'.format(self._accounts[i]['log']))
        self._bitmap = Array('i', bitmap)

    def has_free_session(self):
        return self._busy.value < self._pool_size.value

    def free_sessions_amount(self):
        return self._pool_size.value - self._busy.value

    def is_empty(self):
        return self._pool_size.value > 0

    # this method returns free session
    def get_session(self):
        if self.has_free_session():
            self._lock.acquire()

            # searching for free session in bitmap and returning it
            for i in range(len(self._bitmap)):
                if self._bitmap[i] == 0:
                    self._busy.value += 1
                    session_to_return = self._sessions.__getitem__(i)['session']
                    self._bitmap[i] = 1
                    self._lock.release()
                    return session_to_return

    # this method returns session into the pool
    def return_session(self, session):
        self._lock.acquire()

        for i in range(len(self._sessions)):
            if self._sessions[i]['session'] == session:
                self._bitmap[i] = 0
                self._busy.value -= 1
                break
        self._lock.release()

    # this method closes session and deletes it from the pool
    def close_session(self, session):
        self._lock.acquire()
        for i in range(len(self._sessions)):
            if self._sessions[i]['session'] == session:
                logging.warning('Problems with account {} session with it is closed'.format(self._sessions[i]['log']))
                self._sessions[i]['session'].close()
                # self._sessions.pop(i)
                self._bitmap[i] = -1
                self._pool_size.value -= 1
                self._busy.value -= 1
                break
        self._lock.release()


# this class created to parse one portion of given links
class MyProcess(Process):
    def __init__(self, urls, titles, path):
        super().__init__()
        self._session = pool.get_session()
        self._start_urls = urls
        self._titles = titles
        self._path = path

    def run(self):
        for i in range(len(self._start_urls)):
            link = self._start_urls[i]
            title = self._titles[i].replace('/', '_')
            path = '{}/{}_{}'.format(self._path, link.split('/')[-2].split('-')[-1:][0], title)
            if self.parse(link, path) == CLOSE_PROCESS:
                return

        pool.return_session(self._session)

    def parse(self, url, path):
        if os.path.exists(path):
            return
        os.mkdir(path)
        os.chdir(path)

        while True:
            try:
                content = self._session.get(url, timeout=40).content
                soup = BeautifulSoup(content, 'lxml')
                li_all = soup.find_all('li', {'class': 'content'})
                check_href = li_all[0].find('a').get('href').split('/')[1]
                break
            except requests.exceptions.ReadTimeout:
                continue
            except IndexError:
                pool.close_session(self._session)
                if not pool.has_free_session():
                    logging.error('There is no free sessions process is closing!\n {}\n'.format(path))
                    # todo close process
                    return CLOSE_PROCESS

                self._session = pool.get_session()
                continue

        if check_href != 'in' and check_href != 'pub':
            for li in li_all:
                a = li.find('a')
                link = a.get('href')
                title = a.get('title').replace('/', '_')
                new_path = '{}/{}_{}'.format(path, link.split('/')[-2].split('-')[-1:][0], title)
                self.parse(link, new_path)

        else:
            with open('{}/links.txt'.format(path), 'w', encoding='utf-8') as file:
                for li in li_all:
                    a = li.find('a')
                    link = a.get('href')
                    file.write('{}{}\n'.format(BASIC_URL, link))
        return OK


# this function splits all links on parts for each process
def chunk_links(seq, titles, num):
    avg = len(seq) / float(num)
    links_out = []
    titles_out = []
    last = 0.0

    while last < len(seq):
        links_out.append(seq[int(last):int(last + avg)])
        titles_out.append(titles[int(last):int(last + avg)])
        last += avg

    return [links_out, titles_out]


# this function parse one folder of first level
def parse_folder(link, session, title):
    print('Started parsing of {}\n'.format(title))

    # creating folder
    path = '{}/IDS/{}/{}_{}'.format(ROOT_DIR, LETTER, link.split('/')[-2].split('-')[-1:][0], title)
    if not os.path.exists(path):
        os.mkdir(path)
    os.chdir(path)

    # trying to get page with links
    while True:
        try:
            content = session.get(link, timeout=40).content
            break
        except Exception:
            pool.close_session(session)
            if not pool.has_free_session():
                logging.error('There is no free sessions process is closing!\n {}\n'.format(title))
                return

            session = pool.get_session()


    # collecting urls of next level
    soup = BeautifulSoup(content, 'lxml')
    li_all = soup.find_all('li', {'class': 'content'})
    if len(li_all) == 0:
        logging.warning('Failed to get links in {}'.format(title))
        return

    links = []
    titles = []
    for li in li_all:
        crt_link = li.find('a').get('href')
        crt_title = li.find('a').get('title')
        links.append(crt_link)
        titles.append(crt_title)
        # todo think on size
    parts_amount = PARTS_TO_SPLIT if PARTS_TO_SPLIT < pool.free_sessions_amount() else pool.free_sessions_amount()
    links_and_titles = chunk_links(links, titles, parts_amount)  # one session to get links

    # start all processes
    processes = list()
    for i in range(parts_amount):
        processes.append(MyProcess(links_and_titles[0][i], links_and_titles[1][i], path))
        processes[i].start()

    for crt in processes:
        crt.join()

    print('The {} was successfully parsed \n'.format(title))


def main():
    path = '{}/IDS/{}'.format(ROOT_DIR, LETTER)
    if not os.path.exists(path):
        os.mkdir(path)
    os.chdir(path)

    # trying to get page with links of first level
    session = pool.get_session()
    try:
        content = session.get(HOME_URL.format(symbol=LETTER), timeout=40).content
    except TimeoutError:
        logging.error('Failed to get links of first level')
        return

    # collecting urls of first level
    soup = BeautifulSoup(content, 'lxml')
    li_top_links = soup.find_all('li', {'class': 'content'})
    if len(li_top_links) == 0:
        logging.warning('Failed to get links in letter {}'.format(LETTER))
        return

    print('Start parsing')

    # processing each url of top level
    for li in li_top_links:
        link = li.find('a').get('href')
        title = li.find('a').get('title')
        parse_folder(link, session, title)


pool = SessionsPool(ACCOUNTS)
if __name__ == '__main__':
    main()
