import scrapy
from rumetr.scrapy import ApptItem as Item
import hashlib
import simplejson as json

COMPLEX_NAME = 'ЖК "Ты и Я"'
URL = 'http://you-me.ru'
ADDR = 'ул. Шушенская, 8'
CONST_URL = '&floor_min=1&floor_max=22&rooms=&square_min=29&square_max=134&cost_min=3000000&cost_max=19500000&flat_sort=asc'
HOUSE_NAMES = {
    '1': '1',
    '2': '2',
    '3': '3',
    '4': '4',
    '5': '5',
    '6': '6-7',
    '7': '6-7',
    '8': '8',
    '9': '9',
    '10': '10-13',
    '11': '10-13',
    '12': '10-13',
    '13': '10-13',
    '14': '14',
    '15': '15-16',
    '16': '15-16',
    '17': '17',
    '18': '18',
    '19': '19-22',
    '20': '19-22',
    '21': '19-22',
    '22': '19-22',
}


class Spider(scrapy.Spider):
    name = 'spider'

    start_urls = ['{URL}/flats/?corp={CONST_URL}'.format(URL=URL, CONST_URL=CONST_URL)]

    def parse(self, response):
        complex_id = self._get_hash_by_name(COMPLEX_NAME)

        data = json.loads(response.body)
        for i, flat in enumerate(data):
            house_name = flat['section']
            house_id = self._get_hash_by_name(house_name)
            house_url = '{URL}/buy/section-{section}/'.format(
                URL=URL,
                section=HOUSE_NAMES[house_name],
            )

            yield Item(
                complex_name=COMPLEX_NAME,
                complex_id=complex_id,
                complex_url=URL,
                addr=ADDR,

                house_id=house_id,
                house_name=house_name,
                house_url=house_url,

                id=flat['id'],
                floor=flat['floor'],
                room_count=flat['flat'],
                square=flat['square'].replace(',', '.'),
                price=flat['price'].replace(' ', ''),
                plan_url='{}{}'.format(URL, flat['link']),
            )

    @staticmethod
    def _get_hash_by_name(name):
        return hashlib.md5(name.encode('utf-8')).hexdigest()
