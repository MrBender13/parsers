# coding=utf-8
import scrapy
from rumetr.scrapy import ApptItem as Item
import hashlib
import simplejson as json

COMPLEX_NAME = 'ЖК "ПИКАССО"'
COMPLEX_ADDR = 'ул. Озерная, 35'


class Spider(scrapy.Spider):
    name = 'spider'

    home_url = 'http://picasso-dom.ru'
    start_urls = ['http://picasso-dom.ru/flats/?corp=&floor_min=2&floor_max=22&rooms=&square_min=37&square_max=106&cost_min=5700000&cost_max=21000000&flat_sort=asc']

    def parse(self, response):
        complex_id = self.generate_id(COMPLEX_NAME)
        data = json.loads(response.body)

        for flat in data:
            yield Item(
                complex_name=COMPLEX_NAME,
                complex_id=complex_id,
                complex_url='{}/buy'.format(self.home_url),
                addr=COMPLEX_ADDR,

                house_id=self.generate_id(flat['corp']),
                house_name=flat['corp'],
                house_url='{}/buy'.format(self.home_url),

                id=flat['id'],
                floor=flat['floor'],
                room_count=flat['flat'],
                square=flat['square'].replace(',', '.'),
                price=flat['price'].replace(' ', ''),
                plan_url='{}{}'.format(self.home_url, flat['link']),
            )

    @staticmethod
    def generate_id(string):
        return hashlib.md5(string.encode('utf-8')).hexdigest()
