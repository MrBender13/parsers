# coding=utf-8
import scrapy
from rumetr.scrapy import ApptItem as Item
import hashlib

COMPLEX_NAME = 'ЖК Альтаир'
URL = 'http://kvartirahimki.ru/params.html'
ADDR = 'г. Химки, ул. Зеленая, 6к1'
HOUSE_NAME = '6к1'


class Spider(scrapy.Spider):
    name = 'spider'

    def start_requests(self):
        request = scrapy.Request(URL, meta={'cookiejar': 0}, callback=self.parse)

        yield request

    def parse(self, response):
        url = 'http://kvartirahimki.ru/php/paramstable.php?room1=1&room2=2&room3=3&floor1=2&floor2=25'

        request = scrapy.Request(
            url,
            meta={
                'cookiejar': response.meta['cookiejar'],
                'dont_obey_robotstxt': True,
            },
            callback=self.parse_flats,
        )

        yield request

    def parse_flats(self, response):
        complex_id = self._get_hash_by_name(COMPLEX_NAME)
        house_id = self._get_hash_by_name(HOUSE_NAME)

        for flat in response.xpath('//tbody/tr'):
            flat_data = flat.xpath('.//td/text()').extract()
            yield Item(
                complex_name=COMPLEX_NAME,
                complex_id=complex_id,
                complex_url=URL,
                addr=ADDR,

                house_id=house_id,
                house_name=HOUSE_NAME,
                house_url=URL,

                id=flat_data[1],
                floor=flat_data[0],
                room_count=flat_data[2],
                square=flat_data[3],
                price=flat_data[4].split(' ')[0],
            )

    @staticmethod
    def _get_hash_by_name(name):
        return hashlib.md5(name.encode('utf-8')).hexdigest()
